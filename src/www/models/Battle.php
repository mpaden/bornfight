<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 20.03.19.
 * Time: 12:26
 */



class Battle
{
    public $battleLog;
    public $army1;
    public $army2;

    /**
     * Battle constructor.
     * @param Army $army1
     * @param Army $army2
     */
    public function __construct(Army $army1,Army $army2)
    {
        $this->battleLog = [];
        $this->army1 =$army1;
        $this->army2 =$army2;
    }


    public function run()
    {

        while (count($this->army1->getAliveUnits()) > 0 && count($this->army2->getAliveUnits()) > 0) {

            $this->army1->general->issueOrders($this->army1);
            $this->army2->general->issueOrders($this->army2);

            foreach ($this->army1->units as $unit) {
                $resultString = $unit->executeOrder($this->army2);

                $this->log($resultString);

                if (count($this->army2->getAliveUnits()) == 0) {
                    $this->log($this->army1->name . ' won the battle!');
                    return $this->army1;
                }

            }

            foreach ($this->army2->units as $unit) {
                $resultString = $unit->executeOrder($this->army1);

                $this->log($resultString);
                if (count($this->army1->getAliveUnits()) == 0) {
                    $this->log($this->army2->name . ' won the battle!');
                    return $this->army2;
                }
            }

        }
    }

    public function printLog()
    {

        foreach ($this->battleLog as $line) {
            echo $line . '<br>';
        }
    }

    public function log($line)
    {
        $this->battleLog[] = $line;
    }
    public function reset(){

        $this->army1->reset();
        $this->army2->reset();
    }

}