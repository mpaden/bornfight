<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 18.03.19.
 * Time: 14:07
 */


class Army
{


    public $units;
    public $general;
    public $name;

    /**
     * Army constructor.
     * @param $name
     * @param int $num
     * @param string $generalType
     */
    public function __construct($name, $num = 50, $generalType = 'balanced')
    {
        $this->units = Unit::generateUnits($num);
        $this->general = GeneralFactory::createGeneral($generalType);
        $this->name = $name;
    }


    public function getRandomUnit()
    {
        $aliveUnits = $this->getAliveUnits();

        return array_values($aliveUnits)[rand(0, count($aliveUnits) - 1)];
    }

    public function getAliveUnits()
    {
        return array_filter($this->units, function ($unit) {
            return $unit->alive == true;
        });
    }

    /**
     * Prints text output
     */
    public function salute()
    {
        foreach ($this->units as $unit) {
            $unit->salute();
        }
    }

    public function reset()
    {
        foreach ($this->units as $unit) {
            $unit->reset();
        }
    }

    public function __toString()
    {
        $s = "";
        foreach ($this->units as $unit){
            $s .= $unit;
            $s .= '<br>';
        }
        return $s;
    }

}