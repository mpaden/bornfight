<?php



/**
 * Created by PhpStorm.
 * User: marko
 * Date: 19.03.19.
 * Time: 17:39
 */

class AggressiveGeneral implements IGeneral
{

    /**
     * AggressiveGeneral constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param Army $army
     */
    public function issueOrders($army)
    {
        foreach ($army->getAliveUnits() as $unit) {

            $rollDice = rand(0, 100) / 100;

            if ($rollDice > 0.5) {
                $unit->setOrder(Orders::attackOrder());
            } else {
                $unit->setOrder(Orders::defenseOrder());
            }
        }
    }
}