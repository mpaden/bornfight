<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 19.03.19.
 * Time: 16:07
 */


class Unit
{

    public static $idCounter = 1;

    public static function generateUnits($num)
    {
        $units = [];
        for ($i = 0; $i < $num; $i++) {
            $units[] = new Unit();
        }
        return $units;
    }


    public $id;
    public $health;
    public $alive;
    public $defense;
    private $order;
    public $skill;

    /**
     * Unit constructor.
     * @param int $health
     * @param int $defense
     * @param null $skill
     */
    public function __construct($health = 100,$defense = 0, $skill = null)
    {
        $this->health = $health;
        $this->defense = $defense;
        if($skill == null){
            $this->skill = rand(20, 80);
        }
        $this->id = Unit::$idCounter;
        $this->alive = true;
        Unit::$idCounter++;
    }


    public function setOrder($order)
    {
        $this->order = $order;
    }

    public function executeOrder($enemyArmy)
    {
        $order = $this->order;
        return $order($enemyArmy, $this);
    }

    /**
     * @param $damage
     * @return int
     * returns 0 if unit took no damage, 100 if unit died, otherwise damage taken
     */
    public function takeDamage($damage)
    {
        $damage = max(0, $damage - $this->defense);
        if ($damage == 0) {
            return 0;
        }

        $damageTaken = $this->health - $damage;
        $this->health = max(0, $damageTaken);
        if ($this->health == 0) {
            $this->alive = false;
            return 100;
        }
        return $damageTaken;
    }

    public function salute()
    {
        echo "Unit number " . $this->id . ", ready!<br>";
    }

    public function __toString()
    {
        return "Unit ".$this->id.", health= ".$this->health.", skill=".$this->skill;
    }


    public function reset(){
        $this->health = 100;
    }
}