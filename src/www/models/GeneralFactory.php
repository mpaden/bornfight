<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 19.03.19.
 * Time: 16:34
 */



class GeneralFactory
{
    public static function createGeneral($type)
    {
        switch ($type)
        {
            // TODO more generals
            case 'balanced':
                return new BalancedGeneral();
            case 'aggressive':
                return new AggressiveGeneral();
            default:
                echo 'Cannot create'.$type.' general';
                return null;
        }
    }

}