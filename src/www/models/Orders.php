<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 20.03.19.
 * Time: 13:02
 */




class Orders
{

    public static function attackOrder(){

        return (function ($enemyArmy, $thisUnit) {
            $thisUnit->defense = 0;
            $enemyUnit = $enemyArmy->getRandomUnit();
            $damage = $enemyUnit->takeDamage(rand(10, $thisUnit->skill));
            if($damage == 0){
                return 'Unit ' . $enemyUnit->id . ' took no damage!';
            }
            elseif ($damage == 100){
                return 'Unit ' . $enemyUnit->id . ' is destroyed!';
            }
            else{
                return 'Unit ' . $enemyUnit->id . ' took ' . $damage . ' damage from unit '.$thisUnit->id.'!';
            }

        });
    }


    public static function defenseOrder(){

        return (function ($enemyArmy, $thisUnit) {
            $thisUnit->defense = 50;
            return 'Unit ' . $thisUnit->id . ' taking defensive stance.';
        });

    }
    //TODO more orders

}