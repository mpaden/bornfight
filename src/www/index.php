<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 18.03.19.
 * Time: 13:21
 */

require "config.php";

//read GET parameters
$num1 = 50;
if (isset($_GET['army1'])) {
    $num1 = $_GET['army1'];
}
$num2 = 50;
if (isset($_GET['army2'])) {
    $num2 = $_GET['army2'];
}

$general1 = 'balanced';
if (isset($_GET['general1'])) {
    $general1 = $_GET['general1'];
}

$general2 = 'aggressive';
if (isset($_GET['general2'])) {
    $general2 = $_GET['general2'];
}

$army1 = new Army('First Army',$num1,$general1);
$army2 = new Army('Second Army',$num2,$general2);

$battle = new Battle($army1,$army2);

$winner = $battle->run();
$battle->printLog();
//$battle->reset();

echo '<hr>';
echo $winner;
