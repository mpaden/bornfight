<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 18.03.19.
 * Time: 15:03
 */

spl_autoload_register(function ($class_name) {
    include 'models/'.$class_name . '.php';
});

spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});