Napraviti mini borbu dvije vojske koja se sastoji samo od programerskog dijela u PHPu bez vidljivog sučelja.
Igra je tekstualni "rat" dvije vojske. Vojske su sačinjene od N vojnika, N se dobiva iz GET parametra sa ?army1=50&army2=48.
Logiku borbe, strukturu vojske, tipove podataka, strukturu klasa, strukturu fileova, ispise ako ih ima, sve dodatne feature i opcije smišljaš sam.
Jedna vlastita implementirana ideja je obavezna (može biti bilo što, generali, random potresi, vojnici polude, baš bilo što).
Sve je dopušteno i u principu nema ograničenja koliko mali ili veliki cijeli program mora biti.
Na kraju je bitno samo da se na neki način vidi koja vojska je pobijedila i zašto. :)


Ocjenjuje se:
Objektno orijentirano programiranje
Kvaliteta strukture datoteka i klasa
Jasnoća kôda
Općeniti elementi ispravno napisanog kôda

NE ocjenjuje se prikaz rezultata te dizajn sučelja.